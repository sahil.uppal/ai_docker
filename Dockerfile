FROM nvcr.io/nvidia/pytorch:22.07-py3

RUN apt-get update && apt-get upgrade -y
RUN apt-get install zip unzip -y

WORKDIR /home/sahil

COPY requirements.txt ./

RUN pip install -r requirements.txt
RUN rm requirements.txt

RUN export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/home/sahil/nvidia:/home/sahil/cuda/targets/x86_64-linux/lib

EXPOSE 8888 8887 80 443

#ENTRYPOINT ["bash"]

#CMD ["jupyter", "notebook", "--ip", "0.0.0.0", "--no-browser", "--allow-root"]
